# second-price


### prerequisites

You need NodeJs up and runnig in your computer.
Alse you need to install the node package manager (NPM) after you are done with installing NodeJS.

### Installation


```sh
$ cd ./second-price/
$ npm install
$ gulp compile
$ node ./dist/main.js
```

### Tests

You can write your own test in the src/testing.ts