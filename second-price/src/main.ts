
export class SecondPrice {
   winnerNames: string[];
   winnerIndex: number;
   multipleWinners: boolean;
   firstPrice: number;
   secondPrice: number;
   display: boolean;
   elementCount: number;

   constructor() { }

   initialisation(display) {
      this.display = display;
      this.winnerNames = undefined;
      this.firstPrice = undefined;
      this.secondPrice = undefined;
      this.winnerIndex = undefined;
      this.multipleWinners = false;
      this.elementCount = 0;
   }

   start(list, display = true) {
      this.initialisation(display);
      if (!list || list.length === 0) {
         return this.displayError('No list given');
      } else if (list.length === 1) {
         return this.displayError('Not enough biders. The auction is canceled');
      }
      this.loop(list);
      this.displayResult();
      return { winner: this.getWinnerName(), price: this.getSecondPrice() }
   }

   loop(list: Bid[]) {
      for (let i of list) {
         i.bids = (i.bids !== null && i.bids !== undefined) ? i.bids : [];
         this.elementCount += i.bids.length;
         i.max = Math.max(...i.bids, 0);
         if (isNaN(i.max)) {
            return this.displayError('We only accept numbers in the list: ');
         }
         if (typeof this.firstPrice === 'undefined') {
            this.winnerNames = [];
            this.firstPrice = i.max;
            this.winnerNames.push(i.name);
            continue;
         }
         this.switchCases(i);
      }
   }

   switchCases(i: Bid) {
      if (i.max > this.firstPrice) {
         this.secondPrice = this.firstPrice;
         this.firstPrice = i.max
         this.winnerNames = [];
         this.winnerNames.unshift(i.name);
         this.multipleWinners = false;
      }
      else if (i.max < this.firstPrice) {
         if (i.max > this.secondPrice || typeof this.secondPrice === 'undefined') {
            this.secondPrice = i.max;
         }
      }
      else if (i.max === this.firstPrice) {
         this.winnerNames.push(i.name);
         this.multipleWinners = true;
      }
   }

   random(max): number {
      return Math.ceil(Math.random() * 10) % max;
   }

   getSecondPrice() {
      return this.secondPrice;
   }

   getWinnerName() {
      if (typeof this.secondPrice == 'undefined') {
         return;
      } else if (typeof this.winnerIndex === 'undefined') {
         this.winnerIndex = this.multipleWinners ? this.random(this.winnerNames.length) : 0;
      }
      return this.winnerNames[this.winnerIndex];
   }

   displayError(msg: string) {
      if (!this.display) {
         return;
      }
      return console.error(msg);
   }

   displayResult() {
      if (this.secondPrice === 0 || typeof this.secondPrice === 'undefined') {
         this.secondPrice = undefined;
         return this.displayError('Not enough biders. The auction is canceled');
      }
      if (!this.display) {
         return;
      }
      console.log('The first price is: ', this.firstPrice);
      console.log('The second price is: ', this.secondPrice);
      console.log('The winner is: ', this.getWinnerName(), '! Congratulation :)');
      console.log('Nb elements: ', this.elementCount);
   }

}

export interface Bid {
   name: string;
   bids: number[];
   max?: number;
}
