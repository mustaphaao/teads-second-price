// You need to recompile when you change something in this file
import { SecondPrice, Bid } from './main';


const test_null: Bid[] = [];

const test_nobids: Bid[] = [
   { name: 'A', bids: [] },
   { name: 'C', bids: [] },
   { name: 'B', bids: [] }
];

const test_onebids: Bid[] = [
   { name: 'A', bids: [] },
   { name: 'C', bids: [5, 4] },
   { name: 'B', bids: [] }
];

const test_onebids_bis: Bid[] = [
   { name: 'A', bids: [0, 0] },
   { name: 'C', bids: [5, 4] },
   { name: 'B', bids: [0] }
];

const test_negative_bid: Bid[] = [
   { name: 'A', bids: [-5, -3] },
   { name: 'C', bids: [-5, -6] },
   { name: 'B', bids: [-333333333330] }
];

const test_first: Bid[] = [
   { name: 'First_user', bids: [5555555555555555555555555555555, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'A0', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 409, 23] },
   { name: 'A1', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A2', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A3', bids: [5, 6, 7, 55555555555555555555555555551, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'B', bids: [5, 9, 7, 100, 55, 97, 55, 3, 33] },
   { name: 'C', bids: [5, 6, 7] },
   { name: 'D', bids: [5, 6, 0, 9] },
   { name: 'E', bids: [5, 6, 8, 0] },
];

const test_last: Bid[] = [
   { name: 'First_user', bids: [555, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'A0', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 409, 23] },
   { name: 'A1', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A2', bids: [5, 6, 7, 555, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A3', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'B', bids: [5, 9, 7, 100, 55, 97, 55, 3, 33] },
   { name: 'C', bids: [5, 6, 7] },
   { name: 'D', bids: [5, 6, 0, 9] },
   { name: 'last_user', bids: [5, 6, 800, 0] },
];


const test_tie: Bid[] = [
   { name: 'maybe4', bids: [555, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'secondP', bids: [500, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 409, 23] },
   { name: 'A1', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A2', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'A3', bids: [5, 6, 7, 123, 44, 99, 33, 5, 12, 4, 55, 09, 23] },
   { name: 'maybe1', bids: [5, 9, 555, 100, 55, 97, 55, 3, 33] },
   { name: 'maybe2', bids: [5, 6, 555] },
   { name: 'D', bids: [5, 6, 0, 9] },
   { name: 'maybe3', bids: [5, 6, 555, 0] },
];

const test_biiiiig: Bid[] = [
   { name: 'user', bids: [555, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'SECOND_PRICE', bids: [8000, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'WINNER_2', bids: [9000, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'WINNER_1', bids: [9000, 8600, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] },
   { name: 'user', bids: [555, 8000, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 6, 7, 123, 44, 99, 33, 5, 12, 4, 555, 09, 23] }
];

const secondPrice = new SecondPrice();
const LINE = '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> ';
const DISPLAY = false;

console.log(LINE + 'test_null');
secondPrice.start(test_null, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_null_bis');
secondPrice.start(null, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_nobids');
secondPrice.start(test_nobids, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_onebid');
secondPrice.start(test_onebids, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_onebid_bis');
secondPrice.start(test_onebids_bis, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_negative_bid');
secondPrice.start(test_negative_bid, DISPLAY);
if (secondPrice.getSecondPrice() === undefined && secondPrice.getWinnerName() === undefined) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}


console.log(LINE + 'test_first_is_winner');
secondPrice.start(test_first, DISPLAY);
if (secondPrice.getSecondPrice() === 55555555555555555555555555551 && secondPrice.getWinnerName() === 'First_user') {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}


console.log(LINE + 'test_last_is_winner');
secondPrice.start(test_last, DISPLAY);
if (secondPrice.getSecondPrice() === 555 && secondPrice.getWinnerName() === 'last_user') {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

console.log(LINE + 'test_tie');
secondPrice.start(test_tie, DISPLAY);
if (secondPrice.getSecondPrice() === 500 &&
   ['maybe1', 'maybe2', 'maybe3', 'maybe4'].indexOf(secondPrice.getWinnerName()) != -1) {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}

/*
A: 2 bids of 110 and 130 euros
B: 0 bid
C: 1 bid of 125 euros
D: 3 bids of 105, 115 and 90 euros
E: 3 bids of 132, 135 and 140 euros
*/
const email_list: Bid[] = [
   { name: 'A', bids: [110, 130] },
   { name: 'B', bids: [] },
   { name: 'C', bids: [125] },
   { name: 'D', bids: [105, 115, 90] },
   { name: 'E', bids: [132, 135, 140] }
]
console.log(LINE + 'email_test');
if ((secondPrice.start(email_list, DISPLAY) as any).price === 130 && secondPrice.getWinnerName() === 'E') {
   console.log('Test OK');
} else {
   console.log('Test KO !!!');
}



console.log(LINE + 'test_biiiiig');
// First price 9000
// Second is 8000
// Winner: WINNER_1 or WINNER_2
for (let i = 0; i < 500000; i++) {
   test_biiiiig.push({ name: 'D', bids: [1, 9, 0, -4] });
}
secondPrice.start(test_biiiiig);
if (secondPrice.getSecondPrice() === 8000 &&
   ['WINNER_2', 'WINNER_1'].indexOf(secondPrice.getWinnerName()) != -1) {
   console.log('Big Test OK');
} else {
   console.log('Big Test KO !!!');
}