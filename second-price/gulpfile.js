const gulp = require('gulp');
const typescript = require('gulp-tsc');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');


// Just saying hello
gulp.task('hello', () => {
   return console.log('gulp is running !');
});

// Bundle the ts into js
gulp.task('ts', function () {
   return gulp.src(['src/main.ts', 'src/tests.ts'])
      .pipe(typescript())
      .pipe(gulp.dest('./tmp/'))
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(gulp.dest('./dist/'));
});

// Main
gulp.task('compile', [
   'ts'
]);
